//Savox ip67 Servo
//Finch 10-3-15



servo_body_length = 41.8+.5;  //padded with .5mm clearance
servo_body_width = 20.2;
servo_body_height = 42.9;

tab_length = 7.5;
tab_thickness = 2.5;
tab_height = 30.5;
tab_width = 20.2+.5;
tab_hole_x_center = 51.1;
tab_hole_y_center = 9.8;

indexer_length = 4.5;
indexer_width = 2.5;
indexer_height = 5.5 + .5;

screw_dia = 5.5;
screw_x = 2.5;

horn_height = 2;
horn_dia = 15;

servo_shaft_x = 31.2;
servo_shaft_dia = 7;
servo_shaft_height = 42.9 + 4.85 + horn_height;  // shaft height plus horn

fn = 60;


module servo(){
	//servo_body
	cube([servo_body_length,servo_body_width,servo_body_height]);
	
	//servo_shaft
	translate([servo_shaft_x,servo_body_width/2,0,])
	cylinder(r=servo_shaft_dia/2,h=servo_shaft_height,$fn=fn);
	
	//place tab1
	translate([-tab_length,(servo_body_width-tab_width)/2,tab_height])
	tab();
	
	//place tab2
	translate([servo_body_length+tab_length,tab_width+(servo_body_width-tab_width)/2,tab_height])
	rotate([0,0,180])
	tab();
	
	//add horn
	translate([servo_shaft_x,servo_body_width/2,servo_shaft_height-horn_height])
	cylinder(r=horn_dia/2,h=horn_height,$fn=fn);
}  //end of servo()

//----------------


module tab(){
difference() {
	union(){
		cube([tab_length,tab_width,tab_thickness]);
		translate([tab_length-indexer_length,tab_width/2-indexer_width/2,0,])
		cube([indexer_length,indexer_width,indexer_height]);
	}

	translate([screw_x,(tab_width-tab_hole_y_center)/2,0-.1])
	cylinder(r=screw_dia/2,h=tab_thickness+.2,$fn=fn);

	translate([screw_x,tab_width-(tab_width-tab_hole_y_center)/2,0-.1])
	cylinder(r=screw_dia/2,h=tab_thickness+.2,$fn=fn);

	}
}  //end of tab()

//---------------

//servo();  //normally commented off, uncomment for viewing
//tab();
	



