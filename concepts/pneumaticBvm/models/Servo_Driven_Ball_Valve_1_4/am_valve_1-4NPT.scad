//American am_valve_14NPT 1/4" Male NPT Ball am_valve_14NPT G100
//Finch 9/28/15

//circular dimensions padded by ~.5mm for fit


shaft_dia = 7.9;
shaft_height = 23.33;

am_valve_14NPT_length = 47;

d1 = 22;
d2 = 24.75;
d3 = 22.8;
d4 = 20.5;
d5 = 22;
d6 = 15.5;


l_d1 = 10;
l_d2 = 12;
l_d3 = 9;
l_d4 = 9;
l_d5 = am_valve_14NPT_length - l_d1 - l_d2 - l_d3 - l_d4 +.1;
h_d6 = shaft_height - 2.75;  //measured to land under valve handle, threaded part extends above this

index_length = 19.5 - 15/2;
index_width = 4.5;

echo (l_d5);

fn = 60;
module am_valve_14NPT(){

	//place d1 segment
	translate([-am_valve_14NPT_length/2-.1,0,0])
	rotate([0,90,0])
	cylinder(r=d1/2,h=l_d1+.2,$fn=fn);
	
	//place d2 segment
	translate([-am_valve_14NPT_length/2+l_d1-.1,0,0])
	rotate([0,90,0])
	cylinder(r=d2/2,h=l_d2+.2,$fn=fn);
	
	//place d3 segment
	translate([-am_valve_14NPT_length/2+l_d1+l_d2-.1,0,0])
	rotate([0,90,0])
	cylinder(r=d3/2,h=l_d3+.2,$fn=fn);
	
	//place d4 segment
	translate([-am_valve_14NPT_length/2+l_d1+l_d2+l_d3-.1,0,0])
	rotate([0,90,0])
	cylinder(r=d4/2,h=l_d4+.2,$fn=fn);
	
	//place d5 segment
	translate([-am_valve_14NPT_length/2+l_d1+l_d2+l_d3+l_d4-.1,0,0])
	rotate([0,90,0])
	cylinder(r=d5/2,h=l_d5+.2,$fn=fn);
	
	//place am_valve_14NPT stem
	cylinder(r=shaft_dia/2,h=shaft_height,$fn=fn);
	
	//place shaft shell
	cylinder(r1=d6/2+.5,r2=d6/2,h=h_d6,$fn=fn);
	
	//place index
	translate([0,-index_width/2,0])
	cube([index_length,index_width,h_d6]);
}  //end of am_valve_14NPT()

//------------------

//am_valve_14NPT();  //normally commented off, uncomment for viewing & debug
