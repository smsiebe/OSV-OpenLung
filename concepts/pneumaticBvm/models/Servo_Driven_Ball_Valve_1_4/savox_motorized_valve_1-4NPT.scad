//Motorized 1/4" Female NPT" Ball Valve
//Finch 9-26-15

use <MCAD/involute_gears.scad>

//include <standard_servo.scad>;
include <am_valve_1-4NPT.scad>;
include <savox_ip67_servo.scad>;

shell_wall_x = 7;
shell_wall_y = 5;
shell_wall_z = 3;
shell_length = am_valve_14NPT_length;
shell_height = d2 +  shell_wall_z*2;
shell_width = d2 +  shell_wall_y*2;

echo ("shell_height = ",shell_height);

valve_shaft_dia = 7.5;
valve_shaft_height = 23.3; // 25.4; //from centerline of horz bores
valve_shaft_flat_width = 5.5;

servo_strut_x = 8;
servo_strut_y = 8 + servo_body_width;
servo_strut_z = 8.7;
servo_strut_hole_dia = 4;


servo_offset_x = (shell_length-servo_body_length)/2;
servo_offset_y = -servo_body_width - shell_width/2;
servo_offset_z = shell_height/2+valve_shaft_height-servo_shaft_height;

servo_drive_x = servo_offset_x + servo_shaft_x;
servo_drive_y = servo_offset_y + servo_body_width/2;
servo_drive_z = servo_offset_z + servo_shaft_height;

valve_offset_x = shell_length/2;
valve_offset_y = 0;
valve_offset_z = shell_height/2;

delta_x = servo_drive_x - valve_offset_x;
delta_y = servo_drive_y - valve_offset_y;

echo(delta_x);
echo(delta_y);

shaft_center_to_center = sqrt(delta_x*delta_x + delta_y*delta_y);
echo ("Center to center = ",shaft_center_to_center);

clamp_screw_dia = 4;

//base parameters
post_screw_x = 4;

post_x = 10;
post_y = 5;
post_z = 27;

plate_x = 20;
plate_y = 50;
plate_z = 7;

post_hole_dia = 4;
mount_hole_dia = 4;

base_length = plate_x-post_screw_x*2+tab_hole_x_center+plate_x;
echo ("base length = ",base_length);

circular_pitch=170;  //gear pitch at pitch diameter

fn = 60;

//-------------------

module shell_1(){

	//half 1
	translate([0,-shell_width/2,0])
	cube([shell_length,shell_width,shell_height/2]);
	
} //end of shell_1()

//-------------------

module shell_2(){

difference(){
	union(){
		//half 2
		translate([0,-shell_width/2,shell_height/2])
		cube([shell_length,shell_width,shell_height/2]);
	
		//place servo mounting struts1
		translate([servo_body_length + (shell_length-servo_body_length)/2+.2,-shell_width/2-servo_body_width,shell_height-servo_strut_z])
		cube([servo_strut_x,servo_strut_y,servo_strut_z]);
	
		//place servo mounting struts2
		translate([-5,-shell_width/2-servo_body_width,shell_height-servo_strut_z])
		cube([servo_strut_x,servo_strut_y,servo_strut_z]);
		}

	//cut servo strut mounting hole1
	translate([shell_length/2-tab_hole_x_center/2,-shell_width/2-servo_body_width/2+tab_hole_y_center/2,0])
	cylinder(r = servo_strut_hole_dia/2,h=60,$fn=fn);

	//cut servo strut mounting hole2
	translate([shell_length/2-tab_hole_x_center/2,-shell_width/2-servo_body_width/2-tab_hole_y_center/2,0])
	cylinder(r = servo_strut_hole_dia/2,h=60,$fn=fn);

	//cut servo strut mounting hole3
	translate([shell_length/2+tab_hole_x_center/2,-shell_width/2-servo_body_width/2+tab_hole_y_center/2,0])
	cylinder(r = servo_strut_hole_dia/2,h=60,$fn=fn);

	//cut servo strut mounting hole4
	translate([shell_length/2+tab_hole_x_center/2,-shell_width/2-servo_body_width/2-tab_hole_y_center/2,0])
	cylinder(r = servo_strut_hole_dia/2,h=60,$fn=fn);

	translate([servo_offset_x,servo_offset_y,servo_offset_z])
	servo();

	

	}
} //end of shell_2()

//------------------

module shell(select){
	difference(){
	
		union(){
			
			if (select == 1 )
				{
					shell_1();
				}

			if (select == 2 )
				{
					shell_2();
				}

			if (select == 3 )
				{
					shell_1();					
					shell_2();
				}

		}
	//cut clamp screws in shell
		clamp_screw_holes();
		translate([valve_offset_x,valve_offset_y,valve_offset_z])
		am_valve_14NPT();
	}
}  //end of shell()

//-----------------


module clamp_screw_holes(){
	//cut clamp screw bore 1
	translate([3,-shell_width/2+3,-.1])
	rotate([0,0,0])
	cylinder(r = clamp_screw_dia/2, h = shell_width + .2,$fn=fn);

	//cut clamp screw bore 2
	translate([shell_length - 3,-shell_width/2+3,-.1])
	rotate([0,0,0])
	cylinder(r = clamp_screw_dia/2, h = shell_width + .2,$fn=fn);

	//cut clamp screw bore 3
	translate([3,shell_width/2-3,-.1])
	rotate([0,0,0])
	cylinder(r = clamp_screw_dia/2, h = shell_width + .2,$fn=fn);

	//cut clamp screw bore 4
	translate([shell_length - 3,shell_width/2-3,-.1])
	rotate([0,0,0])
	cylinder(r = clamp_screw_dia/2, h = shell_width + .2,$fn=fn);

}  //end of clamp_screw_holes()

//-------------------


module valve_gear(){

//color([180,0,0])

	gear(number_of_teeth=41, circular_pitch=circular_pitch, rim_width=2,rim_thickness =5,gear_thickness=5,hub_thickness=2,bore_diameter=18);
	difference(){
		
		cylinder(r=11,h=3,$fn=fn);	
	

		intersection(){
			cylinder(r=valve_shaft_dia/2,h=8,$fn=fn);
			translate([-valve_shaft_flat_width/2,-10/2,0])
			cube([valve_shaft_flat_width,10,20]);
		}
	}
	//place position arrow
	translate([0,-15,0])
	rotate([0,0,-90])
	cylinder(r=3,h=6,$fn=3);
} //end of valve_gear()

//------------------

module servo_gear(){

difference(){
	//color([0,125,0])
	
	gear(number_of_teeth=22, circular_pitch=circular_pitch, rim_width=2,rim_thickness =5,gear_thickness=5,hub_thickness=5,bore_diameter=10);

	translate([-7.5,0,0])
	cylinder(r=.7,h=10,$fn=fn);

	translate([7.5,0,0])
	cylinder(r=.7,h=10,$fn=fn);

	translate([0,-7.5,0])
	cylinder(r=.7,h=10,$fn=fn);

	translate([0,7.5,0])
	cylinder(r=.7,h=10,$fn=fn);


	}
}  //end of servo_gear()

//-----------------

module mounting_bracket(){

difference(){
	union(){
		//place post
		cube([post_x,post_y,post_z]);
		//place plate
		translate([0,-23,0])
		cube([plate_x,plate_y,plate_z]);

		translate([post_x,post_y/2,plate_z])
		mount_gusset();
		}
	//cut holes
	//cut post hole1
	translate([post_screw_x,-.1,(post_z-7)/2+7+11/2])
	rotate([-90,0,0])
	cylinder(r=post_hole_dia/2,h=10,$fn=fn);

	//cut post hole2
	translate([post_screw_x,-.1,(post_z-7)/2+7-11/2])
	rotate([-90,0,0])
	cylinder(r=post_hole_dia/2,h=10,$fn=fn);

	//cut mount hole1
	translate([plate_x-7,-16,-.1])
	cylinder(r=mount_hole_dia/2,h=10,$fn=fn);

	//cut mount hole2
	translate([plate_x-7,20,-.1])
	cylinder(r=mount_hole_dia/2,h=10,$fn=fn);

	

	}

}  //end of mounting_bracket()

//-----------------

module mount_gusset(){

	gusset_z = 20;
	gusset_x = 10;
	gusset_top_y = 5;
	gusset_bot_y = 20;
	
	polyhedron(points = [ [0,-gusset_top_y/2,gusset_z],[0,gusset_top_y/2,gusset_z],[0,gusset_bot_y/2,0],[0,-gusset_bot_y/2,0],[gusset_x,gusset_bot_y/2,0],[gusset_x,-gusset_bot_y/2,0]], faces = [ [0,3,2,1],[0,5,3],[0,1,4,5],[1,2,4],[4,2,3,5] ]);

}  //end of mount_gusset()

//-----------------

module base(){
	
	translate([plate_x,0,0])
	mirror([1,0,0])
	mounting_bracket();
	
	translate([plate_x-post_screw_x*2+tab_hole_x_center,0,0])
	mounting_bracket();
	
	translate([plate_x-post_screw_x,-20,0])
	cube([tab_hole_x_center,5,plate_z]);
	
	translate([plate_x-post_screw_x,15,0])
	cube([tab_hole_x_center,5,plate_z]);


} //end of base()

//-----------------


//show valve
	translate([valve_offset_x,valve_offset_y,valve_offset_z])
	am_valve_14NPT();

//show shell(s) 1 = bottom shell, 2 = top shell, 3 = both
	color([.3,.5,.3])
	shell(3);

//show servo
	color([.4,.4,.6])
	translate([servo_offset_x,servo_offset_y,servo_offset_z])
	color([.5,0.1,.1])
	servo();

//show valve gear
	translate([valve_offset_x,valve_offset_y,valve_offset_z+valve_shaft_height])
	valve_gear();

//show servo gear
	translate([servo_drive_x,servo_drive_y,servo_drive_z])
	rotate([0,0,2])
	servo_gear();


//show mounting base
	color([.9,.6,.6])
	translate([base_length-base_length/2+shell_length/2,-shell_width/2-post_z,14.5])
	rotate([90,0,180])
	base();



